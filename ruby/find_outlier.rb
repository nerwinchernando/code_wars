def find_outlier(integers)
  if integers.length > 2
    odds = integers.select { |number| number.odd? }
    evens = integers.select { |number| number.even? }
    if odds.count > evens.count
      integers.each { |i| return i if i.even? }
    else
      integers.each { |i| return i if i.odd? }
    end
  else
    0
  end
end
